//
//  AppDelegate.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

//
//  AppDelegate.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
//MARK:- Helpers
   func checkedForLoggedInUser() {
       if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
           let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
           let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
           window!.rootViewController = viewController
           window!.makeKeyAndVisible()
       }
   }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print(url)
        
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        
        let host = urlComponents?.host ?? ""
        print(host)
        
//        if host == "secretPage" {
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let setWidget = storyBoard.instantiateViewController(identifier: "SetWidgetViewController") as? SetWidgetViewController
//            setWidget?.deepLinkBitCode = urlComponents?.queryItems?.first?.value
//            setWidget?.isDeepLink = true
//            setWidget?.bit?.bitCode = urlComponents?.queryItems?.first?.value
//            window?.rootViewController = setWidget
//        }
//
        return true
    }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeNavigationVC")
    }
    
//    func getFirebaseToken() {
//        InstanceID.instanceID().instanceID(handler: { (result, error) in
//            if let error = error {
//                print("Error fetching remote instange ID: \(error)")
//            } else if let resultValue = result {
//                FirebaseToken = resultValue.token
//                print("Remote instance ID token: \(resultValue.token)")
//            }
//        })
//    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print("7ossa DidFinishLaunch")
        
        UIView.appearance().semanticContentAttribute = .forceLeftToRight

        UITabBar.appearance().barTintColor = #colorLiteral(red: 0.8549019608, green: 0.3254901961, blue: 0.3607843137, alpha: 1)
        UITabBar.appearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        let systemFontAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12)]
//        UITabBarItem.appearance().setTitleTextAttributes(systemFontAttributes, for: .normal)
        
//        MOLH.shared.activate(true)
        
        // [END register_for_notifications]
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("7ossa applicationWillResignActive")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("7ossa applicationWillEnterForeground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("7ossa applicationWillTerminate")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
}
