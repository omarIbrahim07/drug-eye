//
//  DrugEyeAPIManager.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 9/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class DrugEyeAPIManager: BaseAPIManager {

    func getArrayOfMedicines(basicDictionary params:APIParams , onSuccess: @escaping ([Drug])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_HOME_BITS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (jsonResponse) in
        
        if let jsonArray: [[String : Any]] = jsonResponse as? [[String : Any]] {
            let wrapper = Mapper<Drug>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
//            onFailure(apiError)
        }
//
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getArrayOfGI(basicDictionary params:APIParams , onSuccess: @escaping ([DrugDescription])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_HOME_BITS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (jsonResponse) in
            
            if let jsonArray: [[String : Any]] = jsonResponse as? [[String : Any]] {
                let wrapper = Mapper<DrugDescription>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            //
        }) { (apiError) in
            onFailure(apiError)
        }
    }


}

//if let jsonarray = response.result.value as? [[String: Any]]{
//    //what to do here ?
//    var persons:[Person] = []
//    for userDictionary in jsonarray{
//        guard let id = userDictionary["id"] as? String, let name = userDictionary["name"] as? String else { continue }
//        persons.append(Person(id, name))
//    }
//    //Persons complete.
//}
