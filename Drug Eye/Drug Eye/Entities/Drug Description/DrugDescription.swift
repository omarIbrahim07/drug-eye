//
//  DrugDescription.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 9/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class DrugDescription: Mappable {
    
    var gi: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        gi <- map["GI"]
    }
    
}
