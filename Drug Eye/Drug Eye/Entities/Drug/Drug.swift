//
//  Drug.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 9/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Drug: Mappable {
    
    var drugpos: String?
    var nameEn: String?
    var genericName: String?
    var priceNew: String?
    var corEg: String?
    var pharmacology: String?
    var route: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        drugpos <- map["drugpos"]
        nameEn <- map["NameEnglish"]
        genericName <- map["GenericName"]
        priceNew <- map["Pricenew"]
        corEg <- map["coreg"]
        pharmacology <- map["pharmacology"]
        route <- map["route"]
    }
    
}
