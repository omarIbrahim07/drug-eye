//
//  BaseViewController.swift
//  GameOn
//
//  Created by Hassan on 11/23/18.
//  Copyright © 2018 GameOn. All rights reserved.
//

import UIKit
import Toast_Swift
import PKHUD

class BaseViewController: UIViewController, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    let deviceID = UIDevice.current.identifierForVendor?.uuidString
    
    enum bit: String {
        case currencyRates
        case Prayer
        case Weather
        case empty
    }
    
    // Create our plist file
    let defaults = UserDefaults.standard
    
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Countries.plist")

    let picker : UIImagePickerController? = {
        return UIImagePickerController()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ToastManager.shared.isQueueEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBarWithCloseButton()
    }
    
//    //MARK:- Navigation
//    func presentHomeScreen() {
//        let storyboard : UIStoryboard = UIStoryboard.init(name: "Menu", bundle: nil)
//        let viewController = storyboard.instantiateInitialViewController()!
//        appDelegate.window!.rootViewController = viewController
//    }
    
    //MARK:- Navigation
//    func presentHomeScreen(isPushNotification: Bool = false) {
//        //        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        //        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
//        //        let naviagationController = UINavigationController(rootViewController: viewController)
//        //        appDelegate.window!.rootViewController = naviagationController
//        //        appDelegate.window!.makeKeyAndVisible()
//        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
//            appDelegate.window!.rootViewController = viewController
//            appDelegate.window!.makeKeyAndVisible()
//        }
//    }
    
    
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToHomeBITS() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToDiscoverPage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DiscoverViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToSearchPage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToSettingsPage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SettingViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToScannerPage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ScannerViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

    
    func presentSplashScreen() {
        let mainStoryboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let splashScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginOptionsViewController")
        let navigationController = UINavigationController(rootViewController: splashScreenVC)
        appDelegate.window!.rootViewController = navigationController
    }
    
    //    func showPostDetails(postModel: PostModel, delegateHost: PostDetailsViewControllerDelegate? = nil) {
    //        let storyboard : UIStoryboard = UIStoryboard.init(name: "TabBar", bundle: nil)
    //        let viewController: PostDetailsViewController = storyboard.instantiateViewController(withIdentifier: "PostDetailsViewController") as! PostDetailsViewController
    //        viewController.postModel = postModel
    //        viewController.delegate = delegateHost
    //        self.navigationController?.pushViewController(viewController, animated: true)
    //    }
    
    func showPhotoOption() {
        let optionMenu = UIAlertController(title: nil, message: "ChoosePhoto", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let saveAction = UIAlertAction(title: "TakePhoto", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker!.navigationBar.adjustDefaultNavigationBar()
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera() {
        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        self.view.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
    }
    
    func showError(error: APIError, complete: @escaping (Bool)->Void) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        let messageModified: String = error.message! + "\nPlease press here to retry"
        
        self.view.makeToast(messageModified, duration: 60.0, position: .bottom, title: nil, image: nil, style: style, completion: complete)
    }
    
    func startLoading() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        
        HUD.show(.progress)
    }
    
    func stopLoadingWithSuccess() {
        HUD.hide()
    }
    
    func stopLoadingWithError(error: APIError) {
        HUD.hide()
        showError(error: error)
    }
    
    func showAlert(title: String, message: String?, alertActions: [UIAlertAction]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in alertActions {
            alert.addAction(action)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDeleteConfirmationAlert(content: String, confirmAction: @escaping (UIAlertAction) -> Void) {
        let noAction: UIAlertAction = UIAlertAction(title: "لا", style: .cancel, handler: nil)
        
        let yesAction: UIAlertAction = UIAlertAction(title: "نعم", style: .default, handler: confirmAction)
        
        showAlert(title: "هل تريد مسح \(content)", message: nil, alertActions: [yesAction, noAction])
    }
    
}

