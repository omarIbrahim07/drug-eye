//
//  DrugTableViewCell.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/30/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DrugTableViewCell: UITableViewCell {

    @IBOutlet weak var medicineLabel: UILabel!
    @IBOutlet weak var medicineValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
