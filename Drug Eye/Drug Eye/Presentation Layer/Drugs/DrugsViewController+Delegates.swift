//
//  DrugsViewController+Delegates.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/30/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension DrugsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("HHena")
        backgroundImageView.image = UIImage(named: "lv")
        inputToSearchTextField.text = ""
        self.drugDescriptionLabel.text = ""
        self.medicinePosition = nil
        self.pharma =  nil
        self.genericName = nil
        customSearch = 0
        lastDirective = nil
        imageText = nil
        self.route = nil
        self.drugs = []
        tableView.reloadData()
                
    }
    
//    func textFieldShouldClear(_ textField: UITextField) -> Bool {
//        backgroundImageView.image = UIImage(named: "lv")
//        inputToSearchTextField.text = ""
//        self.drugDescriptionLabel.text = ""
//        self.medicinePosition = nil
//        self.pharma =  nil
//        self.genericName = nil
//        customSearch = 0
//        lastDirective = nil
//        self.drugs = []
//        tableView.reloadData()
//    }

        
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == inputToSearchTextField {
                        
            if inputToSearchTextField.keyboardType == UIKeyboardType.phonePad {
                let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
                let characterSet = CharacterSet(charactersIn: string)
                let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
                
                if newText.count == 0 {
                    print("GG")
                    backgroundImageView.image = UIImage(named: "lv")
                    inputToSearchTextField.text = ""
                    self.drugDescriptionLabel.text = ""
                    self.medicinePosition = nil
                    self.pharma =  nil
                    self.genericName = nil
                    customSearch = 0
                    lastDirective = nil
                    imageText = nil
                    self.route = nil
                    self.drugs = []
                    tableView.reloadData()
                }

                if customSearch == 1 {
                    //                if directive == "al" {
                    if lastDirective != nil {
                        getDrugs(directive: lastDirective!, text: newText ?? "")
                    }
                    //                }
                } else if customSearch == 0 {
                    getDrugs(directive: directive, text: newText ?? "")
                }
                
                return allowedCharacters.isSuperset(of: characterSet)
            }
        }
        
        if inputToSearchTextField.text != "" {
            print(inputToSearchTextField.text)
            let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            
            if newText.count == 0 {
                print("GG")
                backgroundImageView.image = UIImage(named: "lv")
                inputToSearchTextField.text = ""
                self.drugDescriptionLabel.text = ""
                self.medicinePosition = nil
                self.pharma =  nil
                self.genericName = nil
                customSearch = 0
                lastDirective = nil
                imageText = nil
                self.route = nil
                self.drugs = []
                tableView.reloadData()
            }

            if customSearch == 1 {
//                if directive == "al" {
                    if lastDirective != nil {
                        getDrugs(directive: lastDirective!, text: newText ?? "")
                    }
//                }
            } else if customSearch == 0 {
                getDrugs(directive: directive, text: newText ?? "")
            }
        }

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
    }
        
}

extension DrugsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drugs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell: DrugTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DrugTableViewCell") as? DrugTableViewCell {
                        
            let medicine = self.drugs[indexPath.row]
            
            if let name = medicine.nameEn {
                cell.medicineLabel.text = name
            }
            
            if let price = medicine.priceNew, let genericName = medicine.genericName {
                cell.medicineValueLabel.text = price + "  " + genericName
            }
                        
            return cell
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let drug: Drug = self.drugs[indexPath.row]
        
        if let name = drug.nameEn, let company = drug.corEg, let pharma = drug.pharmacology {
//            cell.medicineLabel.text = name
            drugDescriptionLabel.text = "\(name) \n \(company) \n \(pharma)"
        }
        
        if let medicinePos = drug.drugpos {
            self.medicinePosition = medicinePos
        }
        
        if let textImage = drug.nameEn {
            self.imageText = textImage
        }
        
        if let route = drug.route {
            self.route = route
        }
        
//        if self.customSearch == 1 {
//            if self.directive == "al" {
                if let pharma = drug.pharmacology {
                    self.pharma = pharma
                }
//            }
//    else if self.directive == "si" {
                if let genericNAme = drug.genericName {
                    self.genericName = genericNAme
                }
//            }
//        }
    }
}
