//
//  DrugsViewController.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/30/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DrugsViewController: BaseViewController {
    
    let menu: [String] = ["Trade Name", "Trade AnyPos", "Google-style", "Price", "Generic Name", "Pharmacology", "Company"]
    var drugs: [Drug] = [Drug]()
    
    var directive: String = "q"
    var directiveCheck: String = "Trade Name"
    var buttonsCheck: Int = 1
    var customSearch: Int? = 0
    
    var medicinePosition: String?
    
    var pharma: String?
    var genericName: String?
    
    var lastDirective: String?
    var imageText: String?
    var route: String?
    
    @IBOutlet weak var inputToSearchTextField: UITextField!
    @IBOutlet weak var tradeNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var drugDescriptionView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var drugDescriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
//        bindBackgroundImage()
        configureTableView()
        closeKeypad()
    }
    
    func checkDirective(menuItem: String) {
        self.inputToSearchTextField.text = ""
        self.drugDescriptionLabel.text = ""
        self.drugs = []
        self.tableView.reloadData()
        self.customSearch = 0
        if menuItem == menu[0] {
            self.directive = "q"
        } else if menuItem == menu[1] {
            self.directive = "qany"
        } else if menuItem == menu[2] {
            self.directive = "qgoogle"
        } else if menuItem == menu[3] {
            self.directive = "price"
            inputToSearchTextField.keyboardType = UIKeyboardType.phonePad
        } else if menuItem == menu[4] {
            self.lastDirective = nil
            self.directive = "si"
        } else if menuItem == menu[5] {
            self.lastDirective = nil
            self.directive = "al"
        } else if menuItem == menu[6] {
            self.directive = "qcompany"
        }
        if menuItem != menu[3] {
            inputToSearchTextField.keyboardType = UIKeyboardType.default
        }
        if inputToSearchTextField.text != "" {
            getDrugs(directive: directive, text: inputToSearchTextField.text!)
        }
    }
    
    func checkButtons() {
        if buttonsCheck == 1 {
            self.customSearch = 1
            self.lastDirective = "al"
            if inputToSearchTextField.text != "" {
                guard let pharma = self.pharma, pharma.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }

                getDrugs(directive: lastDirective!, text: pharma)
            } else {
                guard let pharma = self.pharma, pharma.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
            }
        } else if buttonsCheck == 2 {
            self.customSearch = 1
            self.lastDirective = "si"
            if inputToSearchTextField.text != "" {
                guard let genericName = self.genericName, genericName.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
                        
                getDrugs(directive: lastDirective!, text: genericName)
            } else {
                guard let genericName = self.genericName, genericName.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
            }
        } else if buttonsCheck == 3 {
            self.customSearch = 1
            self.lastDirective = "qnear"
            if inputToSearchTextField.text != "" {
                guard let genericName = self.genericName, genericName.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
                
                guard let route = self.route, route.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
                        
                getDrugs(directive: lastDirective!, text: genericName + "<>" + route)
            } else {
                guard let genericName = self.genericName, genericName.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
                
                guard let route = self.route, route.count > 0 else {
                    let apiError = APIError()
                    apiError.message = "Choose drug from list before clicking here".localized
                    showError(error: apiError)
                    return
                }
            }
        }
    }
    
    func configureView() {
//        navigationItem.title = "DRUG EYE"
        inputToSearchTextField.delegate = self
    }
    
    func changeTextFieldPlaceHolderColorAndSize(textField: UITextField, placeHolderString: String,fontSize: CGFloat, placeHolderColor: UIColor) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: placeHolderColor, .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }
    
    func closeKeypad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func bindBackgroundImage() {
//        backgroundImageView.kf.setImage(with: URL(fileURLWithPath: "http://www.drugeye.pharorg.com/htmlv/searchpage.jpg"), options: [.alsoPrefetchToMemory])
        backgroundImageView.loadImageFromUrl(imageUrl: "http://www.drugeye.pharorg.com/htmlv/searchpage.jpg")
        if backgroundImageView.image == nil {
            backgroundImageView.image = UIImage(named: "lv")
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "DrugTableViewCell", bundle: nil), forCellReuseIdentifier: "DrugTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
    func setupMenuSheet() {
        
        let sheet = UIAlertController(title: "Filteration".localized, message: "Please choose filteration method".localized, preferredStyle: .actionSheet)
        for menuItem in menu {
            sheet.addAction(UIAlertAction(title: menuItem, style: .default, handler: {_ in
                self.tradeNameLabel.text = menuItem
                self.checkDirective(menuItem: menuItem)
                //                                    self.viewModel.getService(service: service)
            }))
        }
        sheet.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    // MARK:- Networking
    func getDrugs(directive: String, text: String) {
        
        let parameters: [String : AnyObject] = [
            directive : text as AnyObject,
        ]
        
        weak var weakSelf = self
        
//        startLoading()
        
        DrugEyeAPIManager().getArrayOfMedicines(basicDictionary: parameters, onSuccess: { (drugs) in
            
//            self.stopLoadingWithSuccess()
            self.drugs = drugs
            self.tableView.reloadData()
            
        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
            self.drugs = []
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    func goToMedicineDetails(medicinePosition: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MedicineDescriptionViewController") as! MedicineDescriptionViewController
        //        viewController.selectedService = service
        viewController.medicinePosition = medicinePosition
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToHelp() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        //        viewController.selectedService = service
        
        self.present(viewController, animated: true, completion: nil)
        //        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToWebView(textImage: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        //        viewController.selectedService = service
        viewController.medicine = textImage
        
        //        self.present(viewController, animated: true, completion: nil)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToEgyDesigner() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        //        viewController.selectedService = service
        viewController.egy = true
        
        //        self.present(viewController, animated: true, completion: nil)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: - Actions
    @IBAction func tradeNamePopUpMenuButtonIsPressed(_ sender: Any) {
        setupMenuSheet()
    }
    
    @IBAction func helpButtonIsPressed(_ sender: Any) {
        print("Help")
        goToHelp()
    }
    
    @IBAction func altersButtonIsPressed(_ sender: Any) {
        print("Alters")
        buttonsCheck = 1
        checkButtons()
    }
    
    @IBAction func similarsButtonIsPressed(_ sender: Any) {
        print("Similars")
        buttonsCheck = 2
        checkButtons()
    }
    
    
    @IBAction func nearButtonIsPressed(_ sender: Any) {
        print("Similars")
        buttonsCheck = 3
        checkButtons()
    }
    
    @IBAction func imageButtonIsPressed(_ sender: Any) {
        print("Image")
        guard let image = self.imageText, image.count > 0 else {
            let apiError = APIError()
            apiError.message = "Choose drug from list before clicking here".localized
            showError(error: apiError)
            return
        }

        goToWebView(textImage: image)

//        if let image = self.imageText {
//            goToWebView(textImage: image)
//        }
    }
    
    @IBAction func descriptionButtonIsPressed(_ sender: Any) {
        
        guard let medicinePos = self.medicinePosition, medicinePos.count > 0 else {
            let apiError = APIError()
            apiError.message = "Choose drug from list before clicking here".localized
            showError(error: apiError)
            return
        }

        goToMedicineDetails(medicinePosition: medicinePos)
    }
    
    @IBAction func developedByEgyDesignerButtonIsPressed(_ sender: Any) {
        goToEgyDesigner()
    }
    
}
