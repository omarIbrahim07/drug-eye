//
//  HomeViewController.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/30/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
//        bindImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        bindImage()
    }
    
    func configureView() {
        self.view.backgroundColor = greenColour
    }

    func bindImage() {
        backgroundImageView.loadImageFromUrl(imageUrl: "http://www.drugeye.pharorg.com/htmlv/mainpage.jpg")
        if backgroundImageView.image == nil {
            backgroundImageView.image = UIImage(named: "Free")
        }
    }
            
    // MARK:- Actions
    @IBAction func startButtonIsPressed(_ sender: Any) {
        goToDrugs()
    }
    
    @IBAction func onlineButtonIsPressed(_ sender: Any) {
        print("Online")
        goToWebView()
    }
    
    @IBAction func refrenceButtonIsPressed(_ sender: Any) {
        goToRefrence()
    }
    
    // MARK: - Navigation
    func goToDrugs() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DrugsViewController") as! DrugsViewController
//        viewController.selectedService = service

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToRefrence() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RefrenceViewController") as! RefrenceViewController
        //        viewController.selectedService = service
        
        self.present(viewController, animated: true, completion: nil)
    }

    
    func goToWebView() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        viewController.online = true
        
        //        self.present(viewController, animated: true, completion: nil)
        navigationController?.pushViewController(viewController, animated: true)
    }

}
