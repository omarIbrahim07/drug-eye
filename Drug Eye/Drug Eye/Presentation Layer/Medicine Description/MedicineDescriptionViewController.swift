//
//  MedicineDescriptionViewController.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/30/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class MedicineDescriptionViewController: BaseViewController {

    var medicinePosition: String?
    
    @IBOutlet weak var medicineDescriptionLabel: UILabel!
    @IBOutlet weak var moreInfoButtonLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        setMedicineDescription()
        
//        medicineDescriptionLabel.text = ""
    }
    
    func getDrugs(medicinePosition: String) {
        
        let parameters: [String : AnyObject] = [
            "gi" : medicinePosition as AnyObject,
        ]
        
        weak var weakSelf = self
        
        //        startLoading()
        
        DrugEyeAPIManager().getArrayOfGI(basicDictionary: parameters, onSuccess: { (drugs) in
            
            //            self.stopLoadingWithSuccess()
            self.medicineDescriptionLabel.text = drugs[0].gi
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }

    func configureView() {
//        navigationItem.title = "drug eye-details"
        navigationItem.title = ""
        moreInfoButtonLabel.text = "Back"
    }
    
    func setMedicineDescription() {
        if let medicinePosition = self.medicinePosition {
            self.getDrugs(medicinePosition: medicinePosition)
        }
    }
    
    func makeAlert() {
    let alertController = UIAlertController(title: "DRUG EYE", message: "Drug-Drug information \n Disease and pathology \n Diagnosis and clinical \n information \n OTC Drugs guide \n Coming soon in next versions \n wait for us", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "Ok", style: .default) { (action) in
//            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - Navigation
    func goBack() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func backButtonIsPressed(_ sender: Any) {
        goBack()
    }
    
    @IBAction func moreInfoButtonIsPressed(_ sender: Any) {
        print("More Info")
//        makeAlert()
        self.navigationController?.popViewController(animated: true)
    }
    
}
