//
//  RefrenceViewController.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 10/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class RefrenceViewController: BaseViewController {

    @IBOutlet weak var refrenceImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        bindRefrenceImageView()
    }
    
    func bindRefrenceImageView() {
        refrenceImageView.loadImageFromUrl(imageUrl: "http://drugeye.pharorg.com/htmlv/references.jpg")
    }
    
    // MARK: - Navigation
    
}
