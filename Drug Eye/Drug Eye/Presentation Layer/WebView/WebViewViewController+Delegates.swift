//
//  WebViewViewController+Delegates.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/31/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import WebKit

extension WebViewViewController: WKNavigationDelegate {
    
    // MARK:- Zoom in for webview
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let javascript = "var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes');document.getElementsByTagName('head')[0].appendChild(meta);"
          webView.evaluateJavaScript(javascript, completionHandler: nil)
    }
}

extension WebViewViewController: UIScrollViewDelegate {

    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        self.webView.scrollView.setZoomScale(20, animated: false)
    }
}
