//
//  WebViewViewController.swift
//  Drug Eye
//
//  Created by Omar Ibrahim on 8/31/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: BaseViewController {

    var medicine: String?
    var online: Bool? = false
    
    var egy: Bool? = false
    
    var webView: WKWebView!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.scrollView.delegate = self
        webView.scrollView.isScrollEnabled = true

        // Do any additional setup after loading the view.
        if let medicine = self.medicine {
            startSearch(medicine: medicine)
        }
        
        if self.online == true {
            goToOnline()
        }
        
        if self.egy == true {
            goToEgyDesigner()
        }
    }
    
    func goToOnline() {
        let queryString = "http://drugeye.pharorg.com/drugeyeapp/main.aspx"
        guard let queryURL = URL(string: queryString) else {
            return
        }
        print(queryURL)
        //        guard let queryURL = URL(string: "https://www.google.com/imghp?hl=ENsearch?q=\(encodedSearchString)") else { return }
        
        webView.scrollView.zoomScale = 20
        
        let myRequest = URLRequest(url:queryURL)
        webView.load(myRequest)

//        webView.sizeToFit()
    }
    
    func goToEgyDesigner() {
        let queryString = "https://www.egydesigner.com/"
        
        guard let queryURL = URL(string: queryString) else {
            return
        }
        print(queryURL)
        //        guard let queryURL = URL(string: "https://www.google.com/imghp?hl=ENsearch?q=\(encodedSearchString)") else { return }
        
        let myRequest = URLRequest(url:queryURL)
        webView.load(myRequest)
        webView.sizeToFit()
    }
    
    func startSearch(medicine: String) {

        // if there are spaces or other special characters,
        // you'll have to escape them:
        let allowedCharacters = NSCharacterSet.urlFragmentAllowed

        guard let  encodedSearchString  = medicine.addingPercentEncoding(withAllowedCharacters: allowedCharacters)  else { return }

//        let queryString = "https://www.google.com/imghp?search?hl=en&q=\(encodedSearchString)"
        let queryString = "https://www.google.com/search?hl=en&tbm=isch&q=" + encodedSearchString
        guard let queryURL = URL(string: queryString) else {
            return
        }
        print(queryURL)
//        guard let queryURL = URL(string: "https://www.google.com/imghp?hl=ENsearch?q=\(encodedSearchString)") else { return }

        let myRequest = URLRequest(url:queryURL)
            webView.load(myRequest)
    }
    
    // MARK: - Navigation

}
