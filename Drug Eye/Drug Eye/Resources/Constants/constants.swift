//
//  constants.swift
//  GameOn
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation
import UIKit

let GoogleClientID = "245060057438-g8g7dhp3emmo5cjdvfijcjarovqmev76.apps.googleusercontent.com"

let CurrencyRatesAPIKey = "qxv32rf4y1ywax670kw5crb7traflm2216tq3ln6fc7n586vp35or8u15lm4"
let WetherAPIKey = "4600b63fbe83d679f2b7ea230eeba02b"

let googleApiKey = "AIzaSyC2NwMXeCXPtLtmEz4DfydiRIS2WLDhayg"
//let googleApiKey = "AIzaSyA0HO7YstPeOF_8_RUyJUxGOb_ANUXkyp8"


//MARK:- User default keys
let kAuthorization = "UserDefaults_Authorization"
let kUserData = "UserDefaults_UserData"

var FirebaseToken : String!

let uuid = NSUUID().uuidString
let kUserDefault = UserDefaults.standard

let appDelegate = UIApplication.shared.delegate as! AppDelegate


let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height


let iPhoneXNavigationBarHeight: CGFloat = 88
let normalNavigationBarHeight: CGFloat = 64

let mainBlueColor = UIColor().hexStringToUIColor(hex: "#4367B2")

let generalError = "An Error has occured"
let ALL_COLLECTIONVIEW_CELL = 1

let nckSelectHomeTab = "nckSelectHomeTab"

let nckUpdatePost = "nckUpdatePost"

let ncSelectHomeTab = Notification.Name(nckSelectHomeTab)

let ncUpdatePost = Notification.Name(nckUpdatePost)

let AppStoreURL = "https://itunes.apple.com/us/app/"

let greenColour = UIColor.systemGreen
let blackColour = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

let BanfsgyColour = #colorLiteral(red: 0.2901960784, green: 0.2784313725, blue: 0.6352941176, alpha: 1)
let SkyBlueColour = #colorLiteral(red: 0.03137254902, green: 0.4509803922, blue: 0.6588235294, alpha: 1)
let SamnyColour = #colorLiteral(red: 0.8431372549, green: 0.8431372549, blue: 0.8431372549, alpha: 1)
let LightStripColour = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
let WhiteColour = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
let BackgroundColour = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
