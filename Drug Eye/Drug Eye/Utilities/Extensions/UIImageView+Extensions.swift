//
//  UIImageView+Extensions.swift
//  GameOn
//
//  Created by Hassan on 3/22/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func loadImageFromUrl(imageUrl: String, cornerRaduis: CGFloat = 0.0, completion: (()->())? = nil) {
        KingfisherManager.shared.cache.clearMemoryCache()
        KingfisherManager.shared.cache.clearDiskCache()
        KingfisherManager.shared.cache.cleanExpiredDiskCache()
        let url = URL(string: imageUrl)
        let resource = ImageResource(downloadURL: url!)
        let processor = DownsamplingImageProcessor(size: self.frame.size)
            >> RoundCornerImageProcessor(cornerRadius: cornerRaduis)
        self.kf.setImage(
            with: resource,
            placeholder: UIImage(named: "broken-image"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
//                .diskCacheExpiration(.never)
//                .memoryCacheExpiration(.never)
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    completion?()
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
        }
    }
}
