//
//  UILabel+Extension.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font as Any], context: nil).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
}
